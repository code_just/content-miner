var Log = require('log');
var fs = require('fs');
var ContentMinerFile = require('./ContentMinerFile');


/**
 * Classe criada para abstrair a lógica de Log de acordo com a necessidade do
 * Content Miner.
 *
 * @api public
 */
function ContentMinerLog () {
  var level = 'debug'; //Nível mais básico
  
  this.id = this.getId();
  this.directory = './logs';

  this.createDirectory();

  var file = new ContentMinerFile(this.getFilePath());
  file.checkSize();

  this.logger = new Log(level, fs.createWriteStream(this.getFilePath(), {flags: 'a'}));
}

/**
 * Cria um ID para ser usado no log e assim separar as instâncias.
 *
 * @api private
 */
ContentMinerLog.prototype.getId = function() {
  var id = Math.floor((Math.random() * 10) + 1);
  var leftPad = 4;
  while ((id+"").length < leftPad) {
      id = "0" + id;
  }
  return id;
}

/**
 * Retorna o caminho completo do arquivo de log.
 *
 * @api private
 */
ContentMinerLog.prototype.getFilePath = function() {
  return this.directory + '/general.log';
}


/**
 * Cria o diretório onde ficará os arquivos de log.
 *
 * @api private
 */
ContentMinerLog.prototype.createDirectory = function() {
  if (!fs.existsSync(this.directory)){
    fs.mkdirSync(this.directory);
  }
}


/**
 * Guarda os dados em arquivo
 *
 * @api private
 */
ContentMinerLog.prototype.log = function(type, message) {
  //Colocar o ID pois como é assíncrono isso facilita a identificar cada execução
  message = "[" + this.id + "] " + message;
  
  switch (type.toLowerCase()) {
    case 'emergency':
      this.logger.emergency(message);
      break;
    case 'alert':
      this.logger.alert(message);
      break;
    case 'critical':
      this.logger.critical(message);
      break;
    case 'error':
      this.logger.error(message);
      break;
    case 'warning':
      this.logger.warning(message);
      break;
    case 'notice':
      this.logger.notice(message);
      break;
    case 'debug':
      this.logger.debug(message);
      break;
    case 'info':
    default:
      this.logger.info(message);
      break;
  }
}

module.exports = ContentMinerLog;
