var Crawler = require('./Crawler');


/**
 * Classe criada para utilizar parte do Padão de Projeto "Factory Method", assim
 * conseguimos organizar o código e essa classe fica responsável por chamar o
 * método que queremos.
 * Se um dia precisar mudar fica mais fácil alterar o código.
 *
 * @api public
 */
function CrawlerCreator(){
    this.createdCrawler;
}

/**
 * Método que chama o método que queremos da classe de Crawler
 *
 * @param  {Crawler} crawler
 * @api public
 */
CrawlerCreator.prototype.factoryMethod = function(crawler){
    if (crawler instanceof Crawler) {
        this.createdCrawler = crawler;
        this.createdCrawler.crawl();
    } else {
        console.log('Error. Não é ua instancia de Crawler.');
        throw 500;
    }
}

module.exports = CrawlerCreator;
