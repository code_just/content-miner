var fs = require('fs');

/**
 * Classe criada para separar a lógica de arquivo.
 * Verifica o tamanho do arquivo e se alcançar o limite cria um novo.
 *
 * @param  {String} path
 * @param  {Number} limitSize
 * @api public
 */
function ContentMinerFile (path, limitSize) {
    //Caminho do arquivo
    this.path = path;

    //Limite de tamanho do arquivo para criar um novo. Default 1MB
    this.limitSize = limitSize || 1024 * 1000;
}

/**
 * Verifica o tamanho do arquivo e se alcançar o limite cria um novo.
 *
 * @api public
 */
ContentMinerFile.prototype.checkSize = function() {
  if (fs.existsSync(this.path)) {
    var stats = fs.statSync(this.path);

    if (stats.size >= this.limitSize) {
      this.backup();
    }
  }
}

 /**
 * Faz um backup do arquivo para começar outro, assim nunca teremos um único arquivo muito grande.
 *
 * @api private
 */
ContentMinerFile.prototype.backup = function() {
  if (fs.existsSync(this.path)) {
    var fileName = this.path.replace(/^.*[\\\/]/, '');
    
    var filePieces = fileName.split('.');
    filePieces.pop(); //remove o ultimo elemento que é a extensão
    var fileNoExt = filePieces.join('.');
    
    var directory = this.path.replace(fileName, '');
    
    var n = fs.readdirSync(directory).length + 1;
    var counter = 1;
    var files = fs.readdirSync(directory);
    for (i in files) {
        if (files[i].substr(0, fileNoExt.length) == fileNoExt) {
            counter++;
        }
    }

    var newFilePath = this.path.replace(fileNoExt, fileNoExt + '_' + counter);

    fs.renameSync(this.path, newFilePath);
  }
}

module.exports = ContentMinerFile;
