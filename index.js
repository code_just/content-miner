var Factory = require('./Factory');
var CrawlerCreator = require('./CrawlerCreator');


var factory = new Factory();
var creator = new CrawlerCreator();

/**
 * Lista de sites que vão ser "crawleados", para cada item dessa lista uma classe
 * que extende Crawler deve ser criado com as configurações específicas do site.
 */
var sites = ['g1', 'jovemnerd'];

for (i in sites) {
    var crawler = factory.createCrawler(sites[i]);
    creator.factoryMethod(crawler);
}
