var fs = require('fs');
var URL = require('url-parse');
var ContentMinerFile = require('./ContentMinerFile');


/**
 * Classe criada para separar a lógica de gravação do conteúdo.
 * Recebe o host do `site` para criar o nome do arquivo.
 *
 * @param  {String} site
 * @api public
 */
function ContentWriter (site) {
  /**
   * Diretório onde vai ficar os arquivos de conteúdo.
   *
   * @type String
   */
  this.directory = './contents';

  /**
   * Nome do sité. É usado para criar o nome do arquivo.
   *
   * @type String
   */
  this.site = site;

  this.createDirectory();
  
  var file = new ContentMinerFile(this.directory + '/' + this.getFileName());
  file.checkSize();
}

/**
 * Cria o diretório onde ficará os arquivos.
 *
 * @api private
 */
ContentWriter.prototype.createDirectory = function() {
  if (!fs.existsSync(this.directory)){
    fs.mkdirSync(this.directory);
  }
}

/**
 * Retorna o nome do arquivo baseado no host do site.
 *
 * @api private
 */
ContentWriter.prototype.getFileName = function() {
  url = new URL(this.site);
  return url.hostname + '.txt';
}

/**
 * Salva o conteúdo no arquivo.
 *
 * @api public
 */
ContentWriter.prototype.save = function(content) {
  fs.appendFileSync(this.directory + '/' + this.getFileName(), content+'\n');
}

module.exports = ContentWriter;
