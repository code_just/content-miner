var Crawler = require('./Crawler');
var JovemNerdCrawler = require('./JovemNerdCrawler');
var G1Crawler = require('./G1Crawler');


/**
 * Classe criada para utilizar parte do Padão de Projeto "Factory", assim
 * conseguimos organizar o código e essa classe fica responsável por instanciar 
 * a classe certa de acordo com uma string recebida.
 * Se um dia precisar mudar fica mais fácil alterar o código.
 *
 * @api public
 */
function Factory(){
    this.createCrawler = function(site){
        var crawler;

        switch(site.toLowerCase()) {
            case 'jovemnerd':
                crawler = new JovemNerdCrawler();
                break;
            case 'g1':
                crawler = new G1Crawler();
                break;
            default:
                crawler = new Crawler();
                break;
        }
        
        return crawler;
    }
}


module.exports = Factory;
