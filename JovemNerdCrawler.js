var Crawler = require('./Crawler');
var fs = require('fs');


/**
 * Classe filha da Crawler.
 * Foi extendida para definir o baseUrl especifica do Jovem Nerd e também foi
 * extendido o método collectContent com uma regra específica.
 *
 * @api public
 */
function JovemNerdCrawler(){
    this.baseUrl = 'http://jovemnerd.com.br/';
    this.cssSelector = 'section.content > .user-entry';
    Crawler.call(this);
}

JovemNerdCrawler.prototype = Object.create(Crawler.prototype);
JovemNerdCrawler.prototype.constructor = JovemNerdCrawler;


/**
 * Este método foi sobrescrito para remover uma DIV específica do conteúdo
 * do site Jovem Nerd. Após isso chama o método da clase Pai e segue o fluxo
 * normal.
 *
 * @param  {Object} $
 * @api private
 */
JovemNerdCrawler.prototype.collectContent = function($) {
    //Remover uma div que não queremos
    $(this.cssSelector).find('.post-footer').remove().end();
    
    //Chamar o Parent
    Crawler.prototype.collectContent.call(this, $);
}


module.exports = JovemNerdCrawler;