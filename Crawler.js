var request = require('request');
var cheerio = require('cheerio');
var URL = require('url-parse');

var Log = require('./ContentMinerLog');
var ContentWriter = require('./ContentWriter');



/**
 * Classe que vai fazer o crawling das páginas.
 *
 * @api public
 */
function Crawler() {
    this.pagesVisited = this.pagesVisited || {};
    this.numPagesVisited = this.numPagesVisited || 0;
    this.pagesToVisit = this.pagesToVisit || [];
    this.maxPagesToVisit = this.maxPagesToVisit || 1000;
    this.baseUrl = this.baseUrl || '';
    this.logger = new Log();
    
    url = new URL(this.baseUrl);
    this.baseUrl = url.protocol + "//" + url.hostname;

    this.pagesToVisit.push(this.baseUrl);
}


/**
 * Inicia o crawlinging.
 *
 * @api public
 */
Crawler.prototype.crawl = function() {
    if(this.numPagesVisited >= this.maxPagesToVisit) {
        this.logger.log('info', 'Reached max limit of '+ this.maxPagesToVisit +' page(s) to visit.');
        return;
    }

    if (this.pagesToVisit.length > 0){
        var nextPage = this.pagesToVisit.pop();
        if (nextPage in this.pagesVisited) {
            // Páginas já visitadas. Repete o crawl
            this.crawl();
        } else {
            // Nova página que ainda não foi visitada
            this.visitPage(nextPage);
        }
    }
}


/**
 * Visita a `url` e tenta achar o conteúdo e também novos links.
 *
 * @param {String} url
 * @api private
 */
Crawler.prototype.visitPage = function(url) {
    var self = this;

    this.pagesVisited[url] = true;
    this.numPagesVisited++;

    // Faz a requisição HTTP
    this.logger.log('info', "Visiting page " + url);
    request(url, function(error, response, body) {
        // Verifica o código de status (200 = HTTP OK)
        self.logger.log('info', "Status code: " + response.statusCode);
        if(response.statusCode !== 200) {
            self.crawl();
            return;
        }
        // Faz o parse do document body
        var $ = cheerio.load(body);

        //Coleta o conteúdo da página
        self.collectContent($);

        //Coleta novos links para ser "crawleado"
        self.collectInternalLinks($);
        self.crawl();
    });
}


/**
 * Coleta novos links e coloca em uma lista para ser "crawleado"
 *
 * @param {Object} $
 * @api private
 */
Crawler.prototype.collectInternalLinks = function($) {
    var self = this;
    var relativeLinks = $("a[href^='/']");
    this.logger.log('info', "Found " + relativeLinks.length + " relative links on page");
    
    relativeLinks.each(function() {
        var href = $(this).attr('href');
        if (href.substr(0, 2) =='//') {
            self.pagesToVisit.push(href.replace("//", ""));
        } else {
            self.pagesToVisit.push(self.baseUrl + href);
        }


    });

    var absoluteLinks = $("a[href^='http']");
    this.logger.log('info', "Found " + absoluteLinks.length + " absolute links on page");
    
    absoluteLinks.each(function() {
        // Check if the absolute link is for the the same domain. TODO: It's better to use regex or URL from url-parse package
        var href = $(this).attr('href');
        var host = href.substr(0, self.baseUrl.length);

        if (host == self.baseUrl && !href.match(/\.(?:jpg|jpeg|png|css|js|mp3|zip).*/ig)) {
            self.pagesToVisit.push(href);
        }
    });
}


/**
 * Coleta o conteúdo de texto da página baseado no seletor CSS específico
 * e chama a classe para gravar esse texto em aquivo.
 *
 * @param {Object} $
 * @api private
 */
Crawler.prototype.collectContent = function($) {
    var bodyText = $(this.cssSelector).find('script').remove().end().text().replace(/\s+/g, " ");

    if (bodyText.length > 0) {
        var contentWriter = new ContentWriter(this.baseUrl);
        contentWriter.save(bodyText);
    }
}

module.exports = Crawler;
