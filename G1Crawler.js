var Crawler = require('./Crawler');
var fs = require('fs');


/**
 * Classe filha da Crawler.
 * Foi extendida para definir o baseUrl especifica do G1
 *
 * @api public
 */
function G1Crawler(){
    this.baseUrl = 'http://g1.globo.com';
    this.cssSelector = 'div.materia-conteudo';
    Crawler.call(this);
}

G1Crawler.prototype = Object.create(Crawler.prototype);
G1Crawler.prototype.constructor = G1Crawler;

module.exports = G1Crawler;